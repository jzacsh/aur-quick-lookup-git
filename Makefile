PKG_NAME  =  quick-lookup

# helpful default for maintainer of this AUR package
refresh: makepkg
	makepkg --printsrcinfo > .SRCINFO

makepkg:
	makepkg

clean:
	$(RM) ./$(PKG_NAME)-git-*.pkg.tar.zst
	$(RM) -rf ./$(PKG_NAME)/
	$(RM) -rf ./pkg/
	$(RM) -rf ./src/

.PHONY: clean makepkg refresh
